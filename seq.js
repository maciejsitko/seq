// Create sequence from object

function seqobj(objects) {
 var sequence = [];
  var index = 0;
 for(var key in objects) {
  var currObj = {};
  currObj[key] = objects[key];
  sequence.push(currObj);
 }
 sequence = sequence.map(function(e) {
   return {
     _value : e
   };
 });
 for(var i=0; i < sequence.length; i++) {
   sequence[i]._next = sequence[i+1];
 }
 return sequence[0];
}


function seqobjarr(objarr) {
  var sequence = {};

  for(var i=0; i < objarr.length; i++) {
    sequence[i] = {};
    sequence[i]._value = objarr[i];
  }

  for(var j=0; j < objarr.length-1; j++) {
      sequence[j]._next = sequence[j+1];
  }

  return sequence[0];
}


function deseq(objarr) {
  var obj = {};
  objarr.map(function(e,i) {
         obj[key(e)] = e[key(e)];
  });
  return obj;
}

function toarr(o) {
  return [].slice.call(o,0);
}

function type(any, pretty) {
  pretty = pretty || false;
  return pretty ? {}.toString.call(any).split(" ")[1].replace(']','').toLowerCase()
  : {}.toString.call(any);
}

function trampoline(f) {
    while (f && f instanceof Function) {
        f = f.apply(f.context, f.args);
    }
    return f;
}

function trampolineM(f) {
  return f.apply(f.context, f.args);

}

function err(context) {
  throw new Error(type(context[0], true) + ' is the wrong type of input for ' + context.callee.name);
}

var newseq = seqobj({ name: "Edward", surname: "Troll"});

function seq(o) {
  return type(o,true) === 'array' && type(o[0],true) === 'object' ? seqobjarr(o)
       : type(o,true) === 'array' ? o
       : type(o,true) === 'object' ? seqobj(o)
       : err(arguments);
}

function flatten(arr) {
  return [].reduce.call(arr,function(a,b) {
    return a.concat(b);
  });
}

function head(arr) {
  return [].slice.call(arr,0,1);
}

function tail(arr) {
  return [].slice.call(arr,1);
}

function slice() {
  array = flatten(head(arguments));
  args = tail(arguments);
  return args.length > 1 ? [].slice.call(array,args[0],args[1]) : [].slice.call(array,args[0]);
}

function firstseq(seq) {
  return seq._value;
}
function nextseq(seq) {
	return seq._next;
}

function cons(newV, oldV) {
  return [newV].concat(oldV);
}


function concat(a,b) {
  return [].concat.call(a,b);
}

function Seq(v) {
  this.v = v;
}

Seq.prototype.take = function() {
};

function mapobj(objseq,f) {
   function recur(objseq, acc) {
     if(typeof objseq === 'undefined') {
        return acc;
     }
     return cons.bind(null,f(firstseq(objseq)), mapobj(nextseq(objseq),f));
   }
   return trampoline(recur.bind(null,objseq,[]));
}

function take(n,oseq,f) {
  function recur(oseq, acc) {
    if(acc === 0 || typeof oseq === 'undefined') {
    return [];
  }
  var current = firstseq(oseq) || [];
  return concat(f(current),recur(nextseq(oseq),acc-1));
  }
  return recur(oseq,n);

}

function getval(v) {
  return type(v,true) === 'object' ? v[Object.keys(v)] : v;
}

function id(v) {
  return v;
}

function getkey(key,objarr) {
  return objarr.filter(function(e) {
    return Object.keys(e)[0] === key;
  });
}

function map(seq,f) {
  return type(seq, true) === 'object' ? mapobj(seqobj(seq), f) : [].map.call(seq,f);
}

function key(o) {
  return Object.keys(o)[0];
}

function truthy(any) {
  return any ? true : false;
}

function falsy(any) {
   return !truthy(any);
}

function find(arr,options) {
  options = options || false;
  if(type(options, true) === 'object') {
    return arr.filter(function(o) {
      return o[key(options)] === options[key(options)];
    });
  } else {
    return arr[arr.indexOf(options)];
  }
}

function step(oseq,f) {
  return f(oseq) ;
}

function strConcat(str,o) {
  return str + o;
}

var ss = seq({name: "Ed", surname: "Herman"});




function Seq(data) {
  this.data = seq(data);
}

Seq.prototype.map = function(f) {
  return new Map(
    f.bind(null,this.data)
  );
}

function Map(data) {
  Seq.call(this, []);
  this.data = data;
}

Map.prototype =  Object.create(Seq.prototype);

var Data = new Seq([{name: "Ben"},{name: "Jean"},{name: "Jean"},{name: "Jean"}]);

